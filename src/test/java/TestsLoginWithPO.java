import Pages.BasePage;
import Pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static Pages.BasePage.GoToURL;
import static Pages.BasePage.makeScreenOnTestFail;
import static Pages.LoginPage.*;
import static helpers.TestData.*;

public class TestsLoginWithPO {
    public WebDriver driver;
    private LoginPage LoginPage;
    public BasePage BasePage;

    @BeforeMethod
    public void SetUp(){
        BasePage = new BasePage();
        driver = BasePage.initialize_driver();
        LoginPage = new LoginPage(driver);
    }

    @Test(description = "Авторизация пользователя при введении правильного логина и пароля",priority = 1)
    public void Login_with_correct_username_and_password(){
        GoToURL(driver,LoginUrl);
        FillLoginPageFields(ValidUsername,ValidPassword);
        ClickToElement(SubmitButton);
        CheckElementsDisplayed(LoginMessage,WelcomeMessage,LogoutButton);
    }

    @Test(description = "Неудачная попытка авторизации при введении неправильного логина и правильного пароля")
    public void Login_with_incorrect_username(){
        GoToURL(driver,LoginUrl);
        FillLoginPageFields(InvalidUsername,ValidPassword);
        ClickToElement(SubmitButton);
        CheckElementsDisplayed(UsernameInvalidMessage);
    }

    @Test(description = "Неудачная попытка авторизации при введении правильного логина и правильрного пароля")
    public void Login_with_incorrect_password(){
        GoToURL(driver,LoginUrl);
        FillLoginPageFields(ValidUsername,InvalidPassword);
        ClickToElement(SubmitButton);
        CheckElementsDisplayed(PasswordInvalidMessage);
    }

    @AfterMethod
    public void TearDown(ITestResult iTestResult){
        makeScreenOnTestFail(iTestResult);
        driver.quit();
    }
}
