package Pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class LoginPage {
    public WebDriver driver;

    @FindBy(id = "username")
    public static WebElement Username;

    @FindBy(id = "password")
    public static WebElement Password;

    @FindBy(className = "radius")
    public static WebElement SubmitButton;

    @FindBy(xpath = "//div[contains(text(),'You logged into a secure area!')]")
    public static WebElement LoginMessage;

    @FindBy(xpath = "//h4[contains(text(),'Welcome to the Secure Area. When you are done click logout below.')]")
    public static WebElement WelcomeMessage;

    @FindBy(xpath = "//div[contains(text(),'Your username is invalid!')]")
    public static WebElement UsernameInvalidMessage;

    @FindBy(xpath = "//div[contains(text(),'Your password is invalid!')]")
    public static WebElement PasswordInvalidMessage;

    @FindBy(linkText = "Logout")
    public static WebElement LogoutButton;


    public LoginPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    
    @Step("Кликаем по элементу:")
    public static void ClickToElement (WebElement element) {
        element.click();
    }

    @Step("Проверяем наличие элементов:")
    public static void CheckElementsDisplayed(WebElement ... list) {
        for (WebElement element: list) {
            Assert.assertTrue(element.isDisplayed());
        }
    }

    @Step("Вводим в элемент значение {value}")
    public static void FillField(WebElement element, String value) {
        element.sendKeys(value);
    }

    @Step("Заполняем поля логина значением {username}, а пароля значением {password}")
    public static void FillLoginPageFields(String username, String password) {
        Username.sendKeys(username);
        Password.sendKeys(password);
    }
}
