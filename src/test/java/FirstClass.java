import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

public class FirstClass {
    public WebDriver driver;

    @BeforeClass
    public void SetupClass(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://the-internet.herokuapp.com/login");
    }

    @Test(priority = 1)
    public void Login_with_correct_username_and_password(){
        driver.findElement(By.id("username")).sendKeys("tomsmith");
        driver.findElement(By.id("password")).sendKeys("SuperSecretPassword!");
        driver.findElement(By.className("radius")).submit();
        Assert.assertTrue(driver.findElement(By.xpath("//div[contains(text(),'You logged into a secure area!')]")).isDisplayed());
        Assert.assertTrue(driver.findElement(By.xpath("//h4[contains(text(),'Welcome to the Secure Area. When you are done click logout below.')]")).isDisplayed());
        Assert.assertTrue(driver.findElement(By.linkText("Logout")).isDisplayed());
    }
    @Test
    public void Login_with_incorrect_username() {
        driver.findElement(By.id("username")).sendKeys("Privet");
        driver.findElement(By.id("password")).sendKeys("SuperSecretPassword!");
        driver.findElement(By.className("radius")).submit();
        Assert.assertTrue(driver.findElement(By.xpath("//div[contains(text(),'Your username is invalid!')]")).isDisplayed());
    }
    @Test
    public void Login_with_incorrect_password() {
        driver.findElement(By.id("username")).sendKeys("tomsmith");
        driver.findElement(By.id("password")).sendKeys("Nastya");
        driver.findElement(By.className("radius")).submit();
        Assert.assertTrue(driver.findElement(By.xpath("//div[contains(text(),'Your password is invalid!')]")).isDisplayed());
    }
    @AfterClass
    public void TearDown(){
        driver.quit();
    }
}
